#include "usart.h"

#if 1
#pragma import(__use_no_semihosting)
//标准库需要的支持函数
struct __FILE 
{ 
	int handle; 

}; 

FILE __stdout;
//定义_sys_exit避免使用半主机模式
void _sys_exit(int x) 
{ 
	x = x; 
} 
//重定义fputc函数
int fputc(int ch, FILE *f)
{      
	while((USART1->SR&0X40)==0);
    USART1->DR = (u8) ch;      
	return ch;
}
#endif 

 /* 
int fputc(int ch, FILE *f)
{
	USART_SendData(USART1, (uint8_t) ch);

	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET) {}	
   
    return ch;
}
int GetKey (void)  { 

    while (!(USART1->SR & USART_FLAG_RXNE));

    return ((int)(USART1->DR & 0x1FF));
}
*/
	
/******************************************************************************/
unsigned char USART_RX_BUF[USART_REC_LEN];     //接收缓冲，usart.h中定义长度

unsigned short USART_RX_STA=0;       //接收状态标志

unsigned char USART4_RX_BUF[USART_REC_LEN];     //接收缓冲，usart.h中定义长度

unsigned short USART4_RX_STA=0;       //接收状态标志
/******************************************************************************/
void uart_init(unsigned long bound)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1|RCC_APB2Periph_GPIOA, ENABLE);

	//USART1_TX   GPIOA.9 = TX
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //PA9
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//USART1_RX	  GPIOA.10 = RX
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;//PA10
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3;    //抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;         //子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			       //IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);

	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);       //串口1初始化
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);  //使能接收中断
	USART_Cmd(USART1, ENABLE);                      //使能串口1

}
void uart4_init(unsigned long bound)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);//for UART4

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10; //PC10
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;//PC11
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3;    //抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;         //子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			       //IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);

	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(UART4, &USART_InitStructure);       //串口4初始化
	USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);  //使能接收中断
	USART_Cmd(UART4, ENABLE);                      //使能串口4

}
/******************************************************************************/
//void USART1_IRQHandler(void)   //串口1中断程序
//{
//	unsigned char Res;
//	
//	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)  //
//	{
//		Res =USART_ReceiveData(USART1);	//读取接收到的字节
//		
//		if(USART_RX_STA==0)
//		{
//			if(Res==0xFF)
//			{
//				USART_RX_BUF[USART_RX_STA]=Res;
//				USART_RX_STA=1;
//			}
//		}
//		else if(USART_RX_STA==1)
//		{
//			if(Res==0x01)
//			{
//				USART_RX_BUF[USART_RX_STA]=Res;
//				USART_RX_STA=2;
//			}
//			else
//			{
//				USART_RX_STA=0;
//			}
//		}
//		else
//		{
//			USART_RX_BUF[USART_RX_STA]=Res;
//			USART_RX_STA++;
//			if(USART_RX_STA==4)
//			{
//				USART_RX_STA=0;
//			}
//		}
//  }
//}
//void UART4_IRQHandler(void)   //串口4中断程序
//{
//	unsigned char Res;
//	
//	if(USART_GetITStatus(UART4, USART_IT_RXNE) != RESET)  //
//	{
//		Res =USART_ReceiveData(UART4);	//读取接收到的字节
//		
//		if(USART4_RX_STA==0)
//		{
//			if(Res==0xEE)
//			{
//				USART4_RX_BUF[USART4_RX_STA]=Res;
//				USART4_RX_STA=1;
//			}
//		}
//		else if(USART4_RX_STA==1)
//		{
//			if(Res==0x02)
//			{
//				USART4_RX_BUF[USART4_RX_STA]=Res;
//				USART4_RX_STA=2;
//			}
//			else
//			{
//				USART4_RX_STA=0;
//			}
//		}
//		else
//		{
//			USART4_RX_BUF[USART4_RX_STA]=Res;
//			USART4_RX_STA++;
//			if(USART4_RX_STA==4)
//			{
//				USART4_RX_STA=0;
//			}
//		}
//  }
//}
/******************************************************************************/


